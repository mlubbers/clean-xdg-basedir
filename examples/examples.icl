module examples

import StdEnv
import Data.Error
import Text.GenPrint

import System.Environment.XDGBasedir

derive gPrint MaybeError

Start w
	# (io, w) = stdio w
	# (dh, w) = xdgDataHome w
	# io = io <<< "data home      : " <<< printToString dh <<< "\n"
	# (dh, w) = xdgConfigHome w
	# io = io <<< "config home    : " <<< printToString dh <<< "\n"
	# (dh, w) = xdgStateHome w
	# io = io <<< "state home     : " <<< printToString dh <<< "\n"
	# (dh, w) = xdgExecutableHome w
	# io = io <<< "executable home: " <<< printToString dh <<< "\n"
	# (dh, w) = xdgCacheHome w
	# io = io <<< "cache home     : " <<< printToString dh <<< "\n"
	# (dh, w) = xdgRuntimeDir w
	# io = io <<< "runtime dir    : " <<< printToString dh <<< "\n"
	# (dh, w) = xdgDataDirs w
	# io = io <<< "data dirs      : " <<< printToString dh <<< "\n"
	# (dh, w) = xdgConfigDirs w
	# io = io <<< "config dirs    : " <<< printToString dh <<< "\n"
	# (_, w) = fclose io w
	= w
