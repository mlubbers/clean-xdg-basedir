implementation module System.Environment._XDGBasedir

import StdEnv
import Data.Error
import StdMaybe
import System.Environment
import System.FilePath

xdgDataHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgDataHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (fromJust mvar </> ".local" </> "share"), w)

xdgConfigHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgConfigHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (home </> ".local" </> "config"), w)

xdgStateHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgStateHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (home </> ".local" </> "state"), w)

xdgExecutableHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgExecutableHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (home </> ".local" </> "bin"), w)

xdgCacheHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgCacheHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (home </> ".cache"), w)

xdgRuntimeDirDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgRuntimeDirDefault w
	# (mvar, w) = getEnvironmentVariable "UID" w
	| isJust mvar = (Ok ("/run" </> "user" </> fromJust mvar), w)
	= (Error "UID is not defined", w)

xdgDataDirsDefault :: !*World -> *(!MaybeErrorString [FilePath], !*World)
xdgDataDirsDefault w = (Ok ["/usr" </> "local" </> "share", "/usr" </> "share"], w)

xdgConfigDirsDefault :: !*World -> *(!MaybeErrorString [FilePath], !*World)
xdgConfigDirsDefault w = (Ok ["/etc" </> "xdg"], w)
