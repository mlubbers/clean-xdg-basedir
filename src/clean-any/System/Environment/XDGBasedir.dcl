definition module System.Environment.XDGBasedir

from Data.Error import :: MaybeErrorString, :: MaybeError
from System.FilePath import :: FilePath

/**
 * Retrieve a data file
 *
 * First searches in $XDG_DATA_HOME/application/filename
 * Then in every $XDG_DATA_DIRS/application/filename
 *
 * @param application name
 * @param file name
 * @result full path to the data file if found
 */
xdgGetDataFile :: !String !FilePath !*World -> *(!MaybeErrorString FilePath, !*World)

/**
 * Retrieve a config file.
 *
 * First searches in $XDG_CONFIG_HOME/application/filename
 * Then in every $XDG_CONFIG_DIRS/application/filename
 *
 * @param application name
 * @param file name
 * @result full path to the data file if found
 */
xdgGetConfigFile :: !String !FilePath !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG data home directory from $XDG_DATA_HOME if it exists, $HOME/.local/share otherwise.
xdgDataHome :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG config home directory from $XDG_CONFIG_HOME if it exists, $HOME/.config otherwise.
xdgConfigHome :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG state home directory from $XDG_STATE_HOME if it exists, $HOME/.local/state otherwise.
xdgStateHome :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG executable files directory: $HOME/.local/bin.
xdgExecutableHome :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG cache files directory from $XDG_CACHE_HOME if it exists, $HOME/.cache otherwise.
xdgCacheHome :: !*World -> *(!MaybeErrorString FilePath, !*World)

/**
 * Retrieve the XDG cache files directory from $XDG_RUNTIME_HOME if it exists.
 *
 * According to the XDG Base Directory Specification:
 * If $XDG_RUNTIME_DIR is not set applications should fall back to a replacement directory with similar capabilities and print a warning message. Applications should use this directory for communication and synchronization purposes and should not place larger files in it, since it might reside in runtime memory and cannot necessarily be swapped out to disk. 
 */
xdgRuntimeDir :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG data directories from $XDG_DATA_DIRS if it exists, [/usr/local/share,/usr/share] otherwise.
xdgDataDirs :: !*World -> *(!MaybeErrorString [FilePath], !*World)

//** Retrieve the XDG data directories from $XDG_CONFIG_DIRS if it exists, [/etc/xdg] otherwise.
xdgConfigDirs :: !*World -> *(!MaybeErrorString [FilePath], !*World)
