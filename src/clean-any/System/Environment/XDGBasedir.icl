implementation module System.Environment.XDGBasedir

import StdEnv
import Data.Error
import StdMaybe
import System.Environment
import System.File
import System.FilePath
import Text

import System.Environment._XDGBasedir

xdgGetDataFile :: !String !FilePath !*World -> *(!MaybeErrorString FilePath, !*World)
xdgGetDataFile app fp w = fallback (app </> fp) xdgDataHome xdgDataDirs w

xdgGetConfigFile :: !String !FilePath !*World -> *(!MaybeErrorString FilePath, !*World)
xdgGetConfigFile app fp w = fallback (app </> fp) xdgConfigHome xdgConfigDirs w

fallback :: !String !(*World -> *(MaybeErrorString FilePath, *World)) !(*World -> *(MaybeErrorString [FilePath], *World)) !*World -> *(!MaybeErrorString FilePath, !*World)
fallback file userdir systemdirs w
	# (mdir, w) = userdir w
	| isError mdir = (liftError mdir, w)
	# (ex, w) = fileExists (fromOk mdir </> file) w
	| ex = (Ok (fromOk mdir </> file), w)
	# (mdirs, w) = systemdirs w
	| isError mdirs = (liftError mdir, w)
	= searchFileIn file (fromOk mdirs) w

searchFileIn :: !FilePath ![FilePath] !*World -> *(!MaybeErrorString FilePath, !*World)
searchFileIn file [] w = (Error (file +++ " not found"), w)
searchFileIn file [f:fs] w
	# path = file </> f
	# (ex, w) = fileExists path w
	| ex = (Ok path, w)
	= searchFileIn file fs w

xdgDataHome :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgDataHome w
	# (mvar, w) = getEnvironmentVariable "XDG_DATA_HOME" w
	| isJust mvar = (Ok (fromJust mvar), w)
	= xdgDataHomeDefault w

xdgConfigHome :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgConfigHome w
	# (mvar, w) = getEnvironmentVariable "XDG_CONFIG_HOME" w
	| isJust mvar = (Ok (fromJust mvar), w)
	= xdgConfigHomeDefault w

xdgStateHome :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgStateHome w
	# (mvar, w) = getEnvironmentVariable "XDG_STATE_HOME" w
	| isJust mvar = (Ok (fromJust mvar), w)
	= xdgStateHomeDefault w

xdgExecutableHome :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgExecutableHome w = xdgExecutableHomeDefault w

xdgCacheHome :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgCacheHome w
	# (mvar, w) = getEnvironmentVariable "XDG_CACHE_HOME" w
	| isJust mvar = (Ok (fromJust mvar), w)
	= xdgCacheHomeDefault w

xdgRuntimeDir :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgRuntimeDir w
	# (mvar, w) = getEnvironmentVariable "XDG_RUNTIME_DIR" w
	| isJust mvar = (Ok (fromJust mvar), w)
	= xdgRuntimeDirDefault w

xdgDataDirs :: !*World -> *(!MaybeErrorString [FilePath], !*World)
xdgDataDirs w
	# (mvar, w) = getEnvironmentVariable "XDG_DATA_DIRS" w
	| isJust mvar = (Ok (split ":" (fromJust mvar)), w)
	= xdgDataDirsDefault w

xdgConfigDirs :: !*World -> *(!MaybeErrorString [FilePath], !*World)
xdgConfigDirs w
	# (mvar, w) = getEnvironmentVariable "XDG_CONFIG_DIRS" w
	| isJust mvar = (Ok (split ":" (fromJust mvar)), w)
	= xdgConfigDirsDefault w
