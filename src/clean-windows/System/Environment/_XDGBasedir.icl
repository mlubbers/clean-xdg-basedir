implementation module System.Environment._XDGBasedir

import StdEnv
import Data.Error
import Data.Tuple
import System.FilePath
import System.OSError

import code from "xdg-basedirC.obj"
import code from library "xdg-basedir-mscvrt"
import code from library "xdg-basedir-shell32"

MAX_PATH :== 260

getFolderData :: (String *World -> *(Bool, *World)) !*World -> *(!MaybeErrorString FilePath, !*World)
getFolderData f w
	# s = createArray MAX_PATH '\0'
	# (ok, w) = f s w
	| not ok
		# (err, w) = getLastOSError w
		= (Error (toString (fromError err)), w)
	= (Ok s, w)	

getLocalAppDataC :: !String !*World -> *(!Bool, !*World)
getLocalAppDataC _ _ = code {
		ccall get_local_app_data "S:I:A"
	}
	
getRoamingAppDataC :: !String !*World -> *(!Bool, !*World)
getRoamingAppDataC _ _ = code {
		ccall get_roaming_app_data "S:I:A"
	}
	
getProgramDataC :: !String !*World -> *(!Bool, !*World)
getProgramDataC _ _ = code {
		ccall get_program_data "S:I:A"
	}

xdgDataHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgDataHomeDefault w = getFolderData getLocalAppDataC w

xdgConfigHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgConfigHomeDefault w = getFolderData getLocalAppDataC w

xdgStateHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgStateHomeDefault w = getFolderData getLocalAppDataC w

xdgExecutableHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgExecutableHomeDefault w = (Error "No default executable home defined", w)

xdgCacheHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgCacheHomeDefault w
	# (merr, w) = getFolderData getLocalAppDataC w
	= case merr of
		Error e = (Error e, w)
		Ok fp = (Ok (fp </> "cache"), w)

xdgRuntimeDirDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgRuntimeDirDefault w = (Error "No default runtime directory defined", w)

xdgDataDirsDefault :: !*World -> *(!MaybeErrorString [FilePath], !*World)
xdgDataDirsDefault w
	# (merr, w) = getFolderData getRoamingAppDataC w
	= case merr of
		Error e = (Error e, w)
		Ok rad
			# (merr, w) = getFolderData getProgramDataC w
			= case merr of
				Error e = (Error e, w)
				Ok pd = (Ok [rad, pd], w)

xdgConfigDirsDefault :: !*World -> *(!MaybeErrorString [FilePath], !*World)
xdgConfigDirsDefault w
	# (merr, w) = getFolderData getRoamingAppDataC w
	= case merr of
		Error e = (Error e, w)
		Ok rad
			# (merr, w) = getFolderData getProgramDataC w
			= case merr of
				Error e = (Error e, w)
				Ok pd = (Ok [rad, pd], w)
