definition module System.Environment._XDGBasedir

from Data.Error import :: MaybeErrorString, :: MaybeError
from System.FilePath import :: FilePath

//** Retrieve the XDG data home directory from $XDGDATAHOME if it exists, $HOME/.local/share otherwise.
xdgDataHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG config home directory from $XDGCONFIGHOME if it exists, $HOME/.config otherwise.
xdgConfigHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG state home directory from $XDGSTATEHOME if it exists, $HOME/.local/state otherwise.
xdgStateHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG executable files directory: $HOME/.local/bin.
xdgExecutableHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG cache files directory from $XDGCACHEHOME if it exists, $HOME/.cache otherwise.
xdgCacheHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)

/**
 * Retrieve the XDG cache files directory from $XDGRUNTIMEHOME if it exists.
 *
 * According to the XDG Base Directory Specification:
 * If $XDGRUNTIMEDIR is not set applications should fall back to a replacement directory with similar capabilities and print a warning message. Applications should use this directory for communication and synchronization purposes and should not place larger files in it, since it might reside in runtime memory and cannot necessarily be swapped out to disk. 
 */
xdgRuntimeDirDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)

//** Retrieve the XDG data directories from $XDGDATADIRS if it exists, [/usr/local/share,/usr/share] otherwise.
xdgDataDirsDefault :: !*World -> *(!MaybeErrorString [FilePath], !*World)

//** Retrieve the XDG data directories from $XDGCONFIGDIRS if it exists, [/etc/xdg] otherwise.
xdgConfigDirsDefault :: !*World -> *(!MaybeErrorString [FilePath], !*World)
