implementation module System.Environment._XDGBasedir

import StdEnv
import Data.Error
import StdMaybe
import System.Environment
import System.FilePath

xdgDataHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgDataHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (fromJust mvar </> "Library" </> "Application Support"), w)

xdgConfigHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgConfigHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (home </> "Library" </> "Application Support"), w)

xdgStateHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgStateHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (home </> "Library" </> "Application Support"), w)

xdgExecutableHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgExecutableHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (home </> ".local" </> "bin"), w)

xdgCacheHomeDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgCacheHomeDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	= case mvar of
		?None = (Error "HOME is not defined", w)
		?Just home = (Ok (home </> "Library" </> "Caches"), w)

xdgRuntimeDirDefault :: !*World -> *(!MaybeErrorString FilePath, !*World)
xdgRuntimeDirDefault w = (Error "No XDG_RUNTIME_DIR default available for mac", w)

xdgDataDirsDefault :: !*World -> *(!MaybeErrorString [FilePath], !*World)
xdgDataDirsDefault w = (Ok ["/Library" </> "Application Support"], w)

xdgConfigDirsDefault :: !*World -> *(!MaybeErrorString [FilePath], !*World)
xdgConfigDirsDefault w
	# (mvar, w) = getEnvironmentVariable "HOME" w
	| isJust mvar = (Ok
		[ fromJust mvar </> "Library" </> "Application Support"
		, "/Library" </> "Application Support"
		, "/Library" </> "Preferences"
		], w)
	= (Error "HOME are not defined", w)
