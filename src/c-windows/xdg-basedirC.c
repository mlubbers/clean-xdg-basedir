#include <windows.h>
#include <shlobj.h>
#include <string.h>
#include <stdbool.h>
#include "Clean.h"

static bool get_folder_path(int i, CleanString s)
{
	TCHAR szPath[MAX_PATH];
	HANDLE hp = GetProcessHeap();
	if (hp == NULL)
		return false;
	HRESULT hr = SHGetFolderPath(NULL, i, NULL, 0, szPath);
	if (hr != S_OK)
		return false;
	CleanStringLength(s) = strlen(szPath);
	strncpy(CleanStringCharacters(s), szPath, MAX_PATH);
	return true;
}

int get_local_app_data(CleanString s)
{
	return get_folder_path(CSIDL_LOCAL_APPDATA, s);
}

int get_roaming_app_data(CleanString s)
{
	return get_folder_path(CSIDL_APPDATA, s);
}

int get_program_data(CleanString s)
{
	return get_folder_path(CSIDL_COMMON_APPDATA, s);
}
