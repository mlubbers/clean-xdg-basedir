# clean-xdg-basedir

A Clean interface to the XDG Base Directory Specification:
https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

On windows and mac it chooses sane alternatives.

## Instructions

To compile and run the examples, run:

```
nitrile build --name=xdg-basedir-examples
./examples/examples
```

## Maintainer

Mart Lubbers (mart@cs.ru.nl)

## License

`xdg-basedir` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).
